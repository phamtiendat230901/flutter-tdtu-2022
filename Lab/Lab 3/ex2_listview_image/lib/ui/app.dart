import 'package:flutter/material.dart';
import '../stream/image_stream.dart';
class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget>{
  String vocabulary = "Click start button to learn new vocabularies";
  LinkImageStream linkImageStream = LinkImageStream();
  List<String> images = [];
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Vocabularies',
      home: Scaffold(
        appBar: AppBar(
          title: Text("Stream Vocabularies"),
        ),
        body: ListView.builder(
            itemCount: images.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Image.network(images[index]),
              );
            }

        ),
        floatingActionButton: FloatingActionButton(
          child: Text('start'),
          onPressed: () {
            changeString();
          },
        ),
      ),
    );
  }

  changeString() async{
    linkImageStream.getImages().listen((event) {
      setState(() {
        images.add(event);
        count++;
      });
    });
  }
}
