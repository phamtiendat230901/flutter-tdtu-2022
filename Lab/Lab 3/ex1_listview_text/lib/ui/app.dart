import 'package:flutter/material.dart';
import '../stream/vocabularies_stream.dart';
class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget>{
  String vocabulary = "Click start button to learn new vocabularies";
  VocabularyStream vocabularyStream = VocabularyStream();
  List<String> data = [];
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Vocabularies',
      home: Scaffold(
        appBar: AppBar(
          title: Text("Stream Vocabularies"),
        ),
        body: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(data[index]),
              );
            }
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('start'),
          onPressed: () {
            changeString();
          },
        ),
      ),
    );
  }

  changeString() async{
    vocabularyStream.getVocabulary().listen((event) {
      setState(() {
        data.add(event);
        count++;
      });
    });
  }
}
