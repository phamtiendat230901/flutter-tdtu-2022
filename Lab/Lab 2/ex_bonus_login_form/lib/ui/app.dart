import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login form",
      home: Scaffold(
        appBar: AppBar(title: Text("Select country form")),
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();

  late String emailAddress;
  late String lastName;
  late String firstName;
  late String birth;
  late String country = "Viet Nam";
  var items = ['Viet Nam'];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
          key: formKey,
          child: Column(
            children: [
              inputEmail(),
              Container(margin: EdgeInsets.only(top: 10)),
              inputlName(),
              Container(margin: EdgeInsets.only(top: 20)),
              inputfName(),
              Container(margin: EdgeInsets.only(top: 30)),
              inputBirth(),
              Container(margin: EdgeInsets.only(top: 40)),
              inputCountry(),
              Container(margin: EdgeInsets.only(top: 50)),
              loginButton(),
            ],
          )),
    );
  }

  Widget inputEmail() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration:
          InputDecoration(icon: Icon(Icons.person), labelText: 'Email address'),
      validator: (value) {
        if (!value!.contains('@')) {
          return 'Pls input valid email.';
        }
        return null;
      },
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget inputlName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.drive_file_rename_outline), labelText: "Lastname"),
      onSaved: (value) {
        lastName = value as String;
      },
    );
  }

  Widget inputfName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.drive_file_rename_outline), labelText: "Firstname"),
      onSaved: (value) {
        firstName = value as String;
      },
    );
  }

  Widget inputBirth() {
    return TextFormField(
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(
          icon: Icon(Icons.access_time_sharp), labelText: "Birthday"),
      onSaved: (value) {
        birth = value as String;
      },
    );
  }

  Widget inputCountry() {
    return DropdownButton(
      value: country,
      icon: const Icon(Icons.map_sharp),
      items: items.map((String items) {
        return DropdownMenuItem(
          value: items,
          child: Text(items),
        );
      }).toList(),
      onChanged: (String? newValue) {
        getCountry();
        setState(() {
          country = newValue!;
        });
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
            print('$emailAddress, $lastName, $firstName, $birth');
          }
        },
        child: Text("Login"));
  }

  getCountry() async {
    var response =
        await http.get(Uri.https("vapi.vnappmob.com", 'api/province'));
    var jsonData = jsonDecode(response.body);
    setState(() {});
  }
}
