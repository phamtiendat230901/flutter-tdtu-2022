import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login App",
      home: Scaffold(
        appBar: AppBar(title: Text("Login form")),
        body: LoginScreen(),

      ),
    );
  }
}

class LoginScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}


class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String lastName;
  late String firstName;
  late String birthYear;
  late String address;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              fieldEmailAddress(),
              Container(margin: EdgeInsets.only(top: 10.0),),
              fieldLastname(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              fieldFirstname(),
              Container(margin: EdgeInsets.only(top: 30.0),),
              fieldBirthyear(),
              Container(margin: EdgeInsets.only(top: 40.0),),
              fieldAddress(),
              Container(margin: EdgeInsets.only(top: 50.0),),
              loginButton()
            ],
          ),
        )

    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return 'Pls input valid email.';
        }
        return null;
      },
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldLastname() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.drive_file_rename_outline),
          labelText: 'Last Name'
      ),
      onSaved: (value) {
        lastName = value as String;
      },
    );
  }

  Widget fieldFirstname() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.drive_file_rename_outline),
          labelText: 'First Name'
      ),
      onSaved: (value) {
        firstName = value as String;
      },
    );
  }

  Widget fieldBirthyear() {
    return TextFormField(
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(
          icon: Icon(Icons.access_time_sharp),
          labelText: 'Birth Year'
      ),
      onSaved: (value) {
        birthYear = value as String;
      },
    );
  }

  Widget fieldAddress() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          icon: Icon(Icons.map),
          labelText: 'Address'
      ),
      onSaved: (value) {
        address = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, $lastName, $firstName, $birthYear, $address');
          }
        },
        child: Text('Login')
    );
  }
}